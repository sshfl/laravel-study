<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AssignmentTest extends TestCase
{
    public function test_post_create_new_assignment()
    {
        $this->post('assignments', ['title' => 'My greate assignment']);

        $this->seeInDatabase('assignment', ['title' => 'My greate assignment']);
    }
}
