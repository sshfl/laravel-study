# sql record

```
ALTER TABLE `demo_district` CHANGE `upid` `upid` CHAR(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '000000' COMMENT '父级 id';

ALTER TABLE `demo_district` CHANGE `id` `id` CHAR(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '000000' COMMENT '主键 id', CHANGE `level` `level` ENUM('1','2','3') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '层级', CHANGE `upid` `pid` CHAR(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '000000' COMMENT '父级id';

ALTER TABLE `demo_district` CHANGE `reorder` `reorder` INT(11) NOT NULL DEFAULT '0' COMMENT '排序，数字越大越靠前';

CREATE TABLE `test`.`demo_event` ( `id` INT NOT NULL AUTO_INCREMENT COMMENT 'primary key' , `name` VARCHAR(255) NOT NULL COMMENT 'event name' , `description` TEXT NOT NULL COMMENT 'event description' , `created_at` DATETIME NOT NULL COMMENT 'data create time' , `updated_at` DATETIME NOT NULL COMMENT '数据最后更新时间' , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT = 'event';

-- alter table
ALTER TABLE `demo_user` CHANGE `username` `name` VARCHAR(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user name';

-- new table
CREATE TABLE `test`.`lts_article` ( `id` INT NOT NULL AUTO_INCREMENT COMMENT '主键id' , `title` VARCHAR(255) NOT NULL COMMENT '标题' , `subtitle` VARCHAR(255) NOT NULL COMMENT '副标题' , `content` LONGTEXT NOT NULL COMMENT '内容' , `description` VARCHAR(255) NOT NULL COMMENT '文章简介' , `created_at` DATETIME NOT NULL COMMENT '数据创建时间' , `updated_at` DATETIME NOT NULL COMMENT '数据最后更新时间' , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT = '文章';

-- new table
CREATE TABLE `test`.`lts_article_comment` ( `id` INT NOT NULL AUTO_INCREMENT COMMENT '主键id' , `article_id` INT UNSIGNED NOT NULL COMMENT '文章id' , `user_id` INT UNSIGNED NOT NULL COMMENT '用户id' , `content` TEXT NOT NULL COMMENT '内容' , `created_at` DATETIME NOT NULL COMMENT '数据创建时间' , `updated_at` DATETIME NOT NULL COMMENT '数据最后更新时间' , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT = '文章评论';

-- add field
ALTER TABLE `lts_article` ADD `user_id` INT UNSIGNED NOT NULL COMMENT '用户id' AFTER `id`;

-- sample table
CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT 'primary key',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'name',
  `name_alias` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名号',
  `name_alias_last_modify_time` int(11) NOT NULL DEFAULT '0' COMMENT '名号最后修改时间',
  `domain_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '域名',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据最后更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户';

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `name_alias`, `name_alias_last_modify_time`, `domain_name`, `created_at`, `updated_at`) VALUES
(1, '1071766043@qq.com', 'qq', '', 0, '', '2016-05-27 08:35:38', '2016-05-27 08:16:55'),
(2, 'sshsfl@yeah.net', 'yeah', '', 0, '', '2016-05-27 08:35:38', '2016-05-27 08:28:22'),
(3, '1071766043@qq.com', 'no2.qq', '', 0, '', '2016-05-27 08:35:38', '2016-05-27 08:16:55'),
(4, '', '', '33为瓣而来', 1464509223, 'aaaa', '2016-05-29 08:29:59', '2016-05-29 00:29:59');

```
