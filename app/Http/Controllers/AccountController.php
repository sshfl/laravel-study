<?php

namespace App\Http\Controllers;

use DB;

use App\User;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function update(Request $request)
    {
        $user = User::find(4);

        $nameAlias = $user->name_alias;
        // 30 days = 2592000 seconds
        $lastModifyAt = $user->name_alias_last_modify_time;

        if (
                ($lastModifyAt != 0)
                &&
                ((time() - $lastModifyAt) > 2592000)
            ) {
            $user->name_alias = $request->name_alias;
            $user->name_alias_last_modify_time = time();
        }

        if ($user->domain_name == '') {
            $this->validate($request, [
                'domain_name' => 'regex:/^[a-zA-Z]\S{0,14}$/'
            ]);
            $user->domain_name = $request->domain_name;
        }

        $user->save();

        return redirect()->route('account_setting');
    }
}
