<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;

class RecipeController extends Controller
{
    public function create()
    {
        return view('tmp.recipes.create');
    }

    public function store(Request $request)
    {
        // $this->validate($request, [
        //     // 'title' => 'required|max:6',
        //     'title' => ['required', 'min:5'],
        //     'body' => 'required',
        //     'author.name' => 'required',
        //     'author.description' => 'required',
        // ]);
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:6',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('recipes/create')
                ->withErrors($validator)
                // session()->getOldInput('{name}')
                ->withInput();
        }
        var_dump('c');exit;
    }
}
