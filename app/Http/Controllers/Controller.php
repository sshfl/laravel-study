<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * 最多显示5个分页码
     */
    const PAGE_NUM_LIMIT = 9;

    const PER_PAGE = 20;

    /**
     * 自定义分页样式
     *
     * @param  object $lists \Illuminate\Pagination\LengthAwarePaginator
     * @return string        分页dom
     */
    public function coustomPagination($lists)
    {
        $currentPage = $lists->currentPage();
        $lastPage = $lists->lastPage();

        if ($currentPage > $lastPage) {
            $currentPage = $lastPage;
        }

        $pageNumLimit = $currentPage + self::PAGE_NUM_LIMIT;
        if ($pageNumLimit > $lastPage) {
            $pageNumLimit = $lastPage + 1;
        }

        $prefixLi = '';
        if ($currentPage > 1) {
            $prefixLi = '<li><a href="'.$lists->url($currentPage - 1).'">上一页</a></li>';
        }

        $li = '<li class="am-active"><a href="javascript:;">'.$currentPage.'</a></li>';
        // notice the if condition '<='
        // not '<'
        if ($lastPage <= 1) {
            $li = '';
        }
        for ($i = ($currentPage + 1); $i < $pageNumLimit; $i++) {
            $li .= '<li><a href="'.$lists->url($i).'">'.$i.'</a></li>';
        }

        $liSuffix = '';
        if ($pageNumLimit < ($lastPage + 1)) {
            $liSuffix = '<li><a href="'.$lists->url($pageNumLimit).'">下一页</a></li>';
        }

        return '<ul class="am-pagination">'.$prefixLi.$li.$liSuffix.'</ul>';
    }

    public function pagination($lists)
    {
        list($currentPage, $lastPage) = $this->paginateArg($lists);

        if ($lastPage === 1) {
            return '';
        }

        if ($lastPage <= self::PAGE_NUM_LIMIT) {
            $li = ($currentPage === 1) ? '' : '<li><a href="'.$lists->previousPageUrl().'">«</a></li>';

            $li .= $this->paginationLi($lists, 1, $lastPage);

            return '<ul class="am-pagination">'.$li.'</ul>';
        }

        if ($currentPage <= (self::PAGE_NUM_LIMIT - 2)) {
            $li = ($currentPage === 1) ? '' :
                '<li><a href="'.$lists->previousPageUrl().'">«</a></li>';

            $li .= $this->paginationLi($lists, 1, (self::PAGE_NUM_LIMIT - 1));

            $li .= '<li><a href="javascript:;">...</a></li>';
            $li .= $this->breakpointLi($lists, $lastPage);
            $li .= '<li><a href="'.$lists->nextPageUrl().'">»</a></li>';
        } elseif ($currentPage > (self::PAGE_NUM_LIMIT - 2) && $currentPage <= ($lastPage - 6)) {
            $li = '<li><a href="'.$lists->previousPageUrl().'">«</a></li>';
            $li .= $this->breakpointLi($lists);
            $li .= '<li><a href="javascript:;">...</a></li>';

            $li .= $this->paginationLi($lists, $currentPage - 2, $currentPage + 2);

            $li .= '<li><a href="javascript:;">...</a></li>';

            $li .= $this->breakpointLi($lists, $lastPage);

            $li .= '<li><a href="'.$lists->nextPageUrl().'">»</a></li>';
        } else {
            $li = '<li><a href="'.$lists->previousPageUrl().'">«</a></li>';
            $li .= $this->breakpointLi($lists);
            $li .= '<li><a href="javascript:;">...</a></li>';
            $li .= $this->paginationLi($lists, ($lastPage - (self::PAGE_NUM_LIMIT - 3)), $lastPage);

            ($currentPage !== $lastPage) && $li .= '<li><a href="'.$lists->nextPageUrl().'">»</a></li>';
        }

        return '<ul class="am-pagination">'.$li.'</ul>';
    }

    public function breakpointLi($lists, $lastPage = null)
    {
        $li = '';

        if (! is_null($lastPage)) {
            $li .= '<li><a href="'.$lists->url($lastPage - 1).'">'.($lastPage - 1).'</a></li>';
            $li .= '<li><a href="'.$lists->url($lastPage).'">'.$lastPage.'</a></li>';
            return $li;
        }

        $li .= '<li><a href="'.$lists->url(1).'">1</a></li>';
        $li .= '<li><a href="'.$lists->url(2).'">2</a></li>';

        return $li;
    }

    protected function paginationLi($lists, $start, $end)
    {
        $li = '';

        for ($i = $start; $i <= $end; $i++) {
            $defaultLi = '<li><a href="'.$lists->url($i).'">'.$i.'</a></li>';
            $activeLi = '<li class="am-active"><a href="'.$lists->url($i).'">'.$i.'</a></li>';
            $li .= ($i === $lists->currentPage())
                ? $activeLi : $defaultLi;
        }

        return $li;
    }

    /**
     * return the pagination argument
     * current page && last page
     *
     * @param  object $lists \Illuminate\Pagination\LengthAwarePaginator
     * @return array  [$currentPage, $lastPage]
     */
    protected function paginateArg($lists)
    {
        $currentPage = $lists->currentPage();
        $lastPage = $lists->lastPage();

        if ($currentPage <= 0) {
            $currentPage = 1;
            return [1, $lastPage];
        }

        if ($currentPage > $lastPage) {
            return [$lastPage, $lastPage];
        }

        return [$currentPage, $lastPage];
    }
}
