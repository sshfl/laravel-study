<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function index()
    {
        // so slowly
        // todo optimize
        $lists = DB::table('district AS d')
            ->select('d.reorder', 'd.id', 'd.name', 'd.level', 'd.pid', 'sd.name AS p_name', 'ssd.name AS pp_name')
            ->leftJoin('district AS sd', 'd.pid', '=', 'sd.id')
            ->leftJoin('district AS ssd', 'sd.pid', '=', 'ssd.id')
            ->take(20)
            ->orderBy('d.id')
            ->paginate(self::PER_PAGE);
            // ->toSql();

        $pagination = $this->pagination($lists);

        $data = [
            'lists' => $lists,
            'pagination' => $pagination,
        ];

        return view('admin.districts.index', $data);
    }
}