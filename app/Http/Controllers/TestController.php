<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\UploadFile;
use App\Http\Requests;

class TestController extends Controller
{
    const UPLOAD_PATH = '../storage/app/public/uploads/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'test controller index';
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'test controller create';
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.tests.show');
        return 'test controller show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        var_dump($id);
        return 'delete tests/{test}';
        //
    }

    public function upload(Request $request)
    {
        $file = $request->file('filename');

        $extension = $file->getClientOriginalExtension();
        $originName = $file->getClientOriginalName();

        $id = uniqid();
        $fileName = $id.'.'.$extension;
        $path = self::UPLOAD_PATH.date('Y').'/'.date('m').'/';
        $result = $file->move($path, $fileName);

        if (! $result) {
            return ['error' => true];
        }

        $uploadFile = new UploadFile;
        $uploadFile->id = $id;
        $uploadFile->filename = $originName;
        $uploadFile->ext = $extension;
        $flag = $uploadFile->save();

        return UploadFile::find($id);
    }

    /**
     * 下载直接通过 laravel download 响应
     *
     * @param  [type] $fileId [description]
     * @return [type]         [description]
     */
    public function download($fileId)
    {
        $file = UploadFile::find($fileId);

        $fileInfo = DB::table('upload_file')->find($fileId);
        $createdAt = substr($fileInfo->created_at, 0, 7);
        $createdAt = str_replace('-', '/', $createdAt).'/';
        $pathToFile = self::UPLOAD_PATH.$createdAt.$fileInfo->id.'.'.$fileInfo->ext;
        return response()->download($pathToFile, $fileInfo->filename);
    }

}
