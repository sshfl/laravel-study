<?php

namespace App\Http\Controllers;

use DB;
use Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $user = new User;

        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $user->save();

        return $user;
    }

    public function index()
    {
        $lists = User::paginate(self::PER_PAGE);

        $pagination = $this->coustomPagination($lists);

        $data = [
            'lists' => $lists,
            'pagination' => $pagination,
        ];

        return view('admin.users.index', $data);
    }

    public function hasPaid()
    {
        return true;
    }

    public function quantity()
    {
        return 2;
    }

    public function fetchItems()
    {
        return ['a', 'b', 'c'];
    }
}