<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\User;
use App\Assignment;
use App\Services\AnalyticsService;

use Illuminate\Http\Request;

// 2016-05-29 13:48:13
Route::group(['prefix' => 'd'], function () {
    Route::get('/', function () {
        return view('d.index');
    });

    Route::put('/interests', 'InterestController@update');
});

Route::get('accounts/1129/settings', function () {
    $user = DB::table('user')
        ->find(4);

    return view('d.setting')->with('user', $user);
})->name('account_setting');

Route::put('accounts/1129', 'AccountController@update');
// 2016-05-29 13:48:19

class Hello
{
    // Fatal error: Call to undefined method Hello::sayWorld()
    // Method sayWorld does not exist.
    use Illuminate\Support\Traits\Macroable;
}

Route::get('anonymous-class', function () {
    //
    $anonymousClass = new class {
        use Illuminate\Support\Traits\Macroable;
    };

    $anonymousClass->macro('foo', function () {
        return 'foo';
    });

    $anonymousClass::macro('bar', function () {
        return 'bar';
    });

    var_dump($anonymousClass->foo());
    var_dump($anonymousClass::bar());
});

Route::get('testbed', function () {
    $hello = new Hello;
    Hello::macro('sayWorld', function () {
        echo 'hello world', '<br />';
    });

    $hello->sayWorld();
    Hello::sayWorld();
});

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'b'], function () {
    Route::get('/', function () {
        return view('admin.index');
    });
    Route::resource('assignments', 'AssignmentController', [
        'parameters' => ['assignments' => 'assignment']
    ]);
    Route::get('articles/{article}/comments/create', function ($id) {
        return view('admin.articles.comment')
            ->with('id', $id);
    });
    Route::post('articles/{article}/comments', 'ArticleCommentController@store');
});

// ---------------------------------------------------

// form request start
Route::get('comments/create', function () {
    return view('tmp.comment_create');
});
Route::post('comments', function (\App\Http\Requests\CreateCommentRequest $request) {

});
// form request end

// Validate start
Route::get('recipes/create', 'RecipeController@create');
Route::post('recipes', 'RecipeController@store');
// Validate end

// upload download start
Route::get('upload', function () {
    return view('tmp.upload');
});
Route::post('upload-file', 'TestController@upload');
Route::get('downloads/{download}', 'TestController@download');
// 第二种下载文件方式
// via nginx
Route::get('another-download/{id}', function ($id) {
    $file = DB::table('upload_file')->find($id);

    $createdAt = str_replace('-', '/', substr($file->created_at, 0, 7));

    $filePath = $createdAt.'/'.$file->id.'.'.$file->ext;

    // 原生 header 设置
    header('X-Accel-Redirect: /protected/'.$filePath);
    // header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$file->filename);
});
Route::get('download_test', function () {
    // 测试了这个受保护的目录是可以不和项目在一个目录里
    $filePath = 'luffy.jpg';
    // header('X-Accel-Redirect: /protected/'.$filePath);
    // header('Content-Disposition: attachment; filename=ly.jpg');
});
// upload download end

// Collecting and Handling User Data start
Route::get('get-route', function () {
    return view('tmp.a');
});
Route::post('post-route', function (Request $request) {
    // var_dump($request->all());
    var_dump(\Request::input('firstName'));
    var_dump(\Request::input('spouse'));
});
Route::post('form', function (Request $request) {
    if (
        $request->hasFile('profile_img') &&
        $request->file('profile_img')->isValid()
    ) {
        var_dump($request->file('profile_img'));
    }

    var_dump($request->file('profile_img')->getError());

    exit;
    var_dump($request->all());
    var_dump($request->input('profile_img'));
    var_dump($request->file('profile_img'));
});
// Collecting and Handling User Data end

Route::get('injecting', function (AnalyticsService $analytics) {
    return view('tmp.injecting')
        ->with('analytics', $analytics);
});

Route::group(['prefix' => 'v3'], function () {
    Route::resource('assignments', 'AssignmentController', ['parameters' => ['assignments' => 'assignment']]);
    Route::get('dashboards', function () {
        $data = [
            (object) array('title' => 'user'),
            (object) array('title' => 'article'),
        ];
        // $data = array();
        return view('admin.tests.dashboard')
            ->with('modules', $data);
    });

    Route::get('passing-data-to-views', function () {
        return view('admin.tests.dashboard')
            ->with('modules', [])
            ->with('key', 'value');
    });
});

// testing start
Route::post('assignments', function (Request $request) {
    $assignment = new Assignment;

    $assignment->title = $request->title;
    $assignment->save();

    return $assignment;
});
Route::get('assignments', function () {
    return Assignment::all();
});
// testing end

// Abort start
Route::get('something-you-cant-do', function () {
    return Response::download('../.gitignore', 'gg');
    return Response::make('h, w');
    abort(403, 'You cannot do that!');
});
// Abort end

// Reditects start
Route::get('redirect-with-facade', function () {
    return Redirect::to('auth/login');
});
Route::get('redirect-with-helper', function () {
    return redirect()->to('auth/login', 301);
});
Route::get('redirect-with-helper-shortcut', function () {
    return redirect('auth/login');
});
Route::get('redirect', function () {
    return Redirect::route('v2.tests.show', [22]);
    // return Redirect::route('v2.tests.show', ['test' => 22]);
});
Route::get('redirect-with-key-value', function () {
    return Redirect::to('dashboard')
        ->with('error', true);
});
Route::get('redirect-with-array', function () {
    return Redirect::to('dashboard')
        ->with(['error' => true, 'message' => 'Whoops']);
});
Route::get('dashboard', function () {
    var_dump('the dashboard page');
    exit;
});
Route::get('auth/login', function () {
    var_dump('Login auth page...');
    exit;
});
// Reditects end

// form route start
Route::get('form-get', function (Request $request) {
    var_dump($request->all());
    var_dump('form get');
});

Route::post('form-post', function (Request $request) {
    var_dump($request->all());
    var_dump('form post');
});

Route::put('form-put', function (Request $request) {
    var_dump($request->all());
    var_dump('form put');
});

Route::delete('form-delete', function (Request $request) {
    var_dump($request->all());
    var_dump('form delete');
});

Route::put('put-test', function (Request $request) {
    var_dump($request->all());
    var_dump('put test');
});

Route::patch('patch-test', function (Request $request) {
    var_dump($request->all());
    var_dump('patch test');exit;
});
// form route end

Route::get('jsons', function () {
    // set the content-type to return json custom
    header('Content-Type: application/json');
    $arr = ['user_name' => 'safarishi', 'user_email' => '12306@qq.com'];

    $arrStr = json_encode($arr);

    exit($arrStr);
});

// Route::resources([
//     'journals' => 'JournalController',
//     'demos' => 'DemoController',
// ]);

Route::group(['prefix' => 'v2'], function () {
    // Named Routes v2.articles.index parameters
    Route::resource(
        'articles',
        'ArticleController',
        ['parameters' => ['articles' => 'article']]
    );

    Route::resource('tests', 'TestController', [
        'parameters' => ['tests' => 'test'],
    ]);

    Route::get('users/{user}', function (User $user, Request $request) {
        var_dump($user);exit;
        return view('admin.users.show')->with('user', $user);
    });

    Route::get('events/{event}', function (\App\Event $event) {
        return view('admin.events.show')->with('event', $event);
    });

    Route::post('tmp/as', function () {
        return [
            'error' => false,
            'data' => ['username' => 'sshfl56', 'age' => 251],
        ];
    });

    Route::delete('tmp/bs', function () {
        return [
            'error' => true,
            'data' => [
                'name' => 'your-name',
                'email' => 'your-email'
            ]
        ];
    });
});

Route::group(['prefix' => 'v1'], function () {
    Route::get('users', 'UserController@index');
    Route::get('indexes', function () {
        return view('admin.index');
    });
    Route::get('districts', 'DistrictController@index');

    // ------------------------------------------
    Route::post('users', 'UserController@store');

    Route::get('users/{user_id}/comments/{comment_id}', [
        'as' => 'users.comments.show',
        function ($userId, $commentId) {
            // todo
        }
    ]);

    Route::get('emails', function () {
        return view('emails.view', ['user' => ['name' => 'safarishi']]);
    });

    Route::get('search/users', function () {
        $users = Searchy::user('username', 'email', 'phone')
            ->select('username', 'email', 'phone')
            ->query('8')
            ->get();

        // todo
    });
});

// ----------------------------------------------
/*
 * :id {:id} is not working
 * todo: study where is set {} in laravel
 */
Route::get('users/{id}/friends', function ($id) {
    //
});

Route::get('members/{id}/edit', [
    'as' => 'members.edit',
    function ($id) {
        // usage: route('members.edit', ['id' => 22]
        // route('members.edit', [565])
    }
]);

/*
 * inject dependencies
 */
Route::get('users/{id}', function (
        $id,
        App $app,
        Request $request
    ) {
    // todo
});

$router->get('abouts', function () {
    return 'abouts';
});

Route::auth();

Route::get('/home', 'HomeController@index');
