<?php

namespace App\Http\Requests;

use DB;

use App\Http\Requests\Request;

class CreateCommentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $articleId = $this->route('article');

        $userId = 10;
        // 判断的条件是用户不可评论自己的文章
        return ! DB::table('article')
            ->where('id', '=', $articleId)
            ->where('user_id', '=', $userId)
            ->exists();

        return false; // Forbidden
        return true; // pass
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'integer',
            'content' => 'required|min:5',
        ];
    }
}
