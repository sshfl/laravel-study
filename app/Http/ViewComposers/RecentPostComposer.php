<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

class RecentPostComposer
{
    public function compose(View $view)
    {
        $view->with('posts', 'p1p2p3');
    }
}