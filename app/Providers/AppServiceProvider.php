<?php

namespace App\Providers;

use Blade;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('i', 'prr');
        view()->composer('tmp.partials.*', function ($view) {
            $view->with('p', 'ps141/..3');
        });
        view()->composer(
            'tmp.cc.c',
            'App\Http\ViewComposers\RecentPostComposer'
        );
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
