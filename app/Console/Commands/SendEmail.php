<?php

namespace App\Console\Commands;

use Mail;
use Illuminate\Console\Command;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to myself.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = [
            'email' => '1071766043@qq.com',
            'name' => 'qq-10717',
        ];

        $status = Mail::send('emails.view', ['user' => $user], function ($msg) use ($user) {
            $msg->from('sshsfl@yeah.net', 'sshsfl yeah net');
            $msg->to($user['email'], $user['name'])
                ->subject('2016-05-06 10:06:02 demo email to myself.');
        });

        if (! $status) {
            $this->error('Fail to send email.');
            return;
        }

        $this->info('Success to send email.');
        return;
    }
}
