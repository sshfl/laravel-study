<!doctype html>
<html class="no-js fixed-layout">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>App Title - @yield('title', 'Index')</title>
  <meta name="description" content="这是一个 index 页面">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="/admin/amazeui/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="/admin/amazeui/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="/admin/amazeui/css/amazeui.min.css"/>
  <link rel="stylesheet" href="/admin/amazeui/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

@include('admin.layouts.header')

<div class="am-cf admin-main">
  @include('admin.layouts.sidebar')

  <!-- content start -->
  <div class="admin-content">
    <div class="admin-content-body">
      @yield('content-body')
    </div>

    @include('admin.layouts.footer')
  </div>
  <!-- content end -->
</div>

<a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"></a>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/admin/amazeui/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="/admin/amazeui/js/amazeui.min.js"></script>
<script src="/admin/amazeui/js/app.js"></script>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      // 'X-CSRF-TOKEN': '{{ csrf_token() }}', // means 1
    }
  });
</script>
@yield('script')
</body>
</html>
