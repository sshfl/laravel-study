<div>
    <p>{{ $event->id }}</p>
    <p>{{ $event->name }}</p>
    <p>{{ $event->description }}</p>
</div>

<form action="/form-get">
    <!--  -->
    <input type="text" name="form_name">
    <input type="submit" value="get">
</form>

<form action="/form-post" method="POST">
    {{ csrf_field() }}
    <input type="email" name="email">
    <input type="submit" value="post">
</form>

<hr>

<form action="/form-put" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
    <input type="password" name="password">
    <input type="submit" value="put">
</form>

<hr>

<form action="/form-delete" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
    <input type="text" name="title">
    <input type="submit" value="delete">
</form>

<hr>

<form action="/put-test" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
    <input type="text" name="article_id" value="article id">
    <input type="submit" value="pt">
</form>

<hr>

<form action="/patch-test" method="POST">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
    <input type="text" name="category_id" value="ci">
    <input type="submit" value="patch">
</form>