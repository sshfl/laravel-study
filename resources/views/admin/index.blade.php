@extends('admin.layouts.master')

@section('content-body')
<div class="am-cf am-padding">
  <ol class="am-breadcrumb">
    @include('admin.layouts.breadcrumb')
    <li class="am-active">列表</li>
  </ol>
</div>

<ul class="am-avg-sm-1 am-avg-md-4 am-margin am-padding am-text-center admin-content-list ">
  <li><a href="/b/assignments" class="am-text-success"><span class="am-icon-btn am-icon-file-text"></span><br/>新任务<br/>2300</a></li>
  <li><a href="#" class="am-text-warning"><span class="am-icon-btn am-icon-briefcase"></span><br/>成交 Success<br/>308</a></li>
  <li><a href="#" class="am-text-danger"><span class="am-icon-btn am-icon-recycle"></span><br/>昨日访问<br/>80082</a></li>
  <li><a href="#" class="am-text-secondary"><span class="am-icon-btn am-icon-user-md"></span><br/>在线用户<br/>3000</a></li>
</ul>
@endsection

@section('script')
<script type="text/javascript">
    $.ajax({
        type: 'post',
        url: '/v2/tmp/as',
        success: function(data) {
            // console.log(data);
        }
    });

    $.ajax({
        type: 'delete',
        url: '/v2/tmp/bs',
        success: function(data) {
            // console.log(data);
        }
    });
</script>
@endsection