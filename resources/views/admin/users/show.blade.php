@extends('admin.layouts.master')

@section('title', 'User')

@section('content-body')
<div class="am-cf am-padding">
  <ol class="am-breadcrumb">
    @include('admin.users.breadcrumb')
    <li class="am-active">个人资料</li>
  </ol>
</div>

<div class="am-g">
  <div class="am-u-sm-12 am-u-md-8 am-u-md-push-12">
    <form class="am-form am-form-horizontal">
      <div class="am-form-group">
        <label for="user-name" class="am-u-sm-3 am-form-label">姓名 / Name</label>
        <div class="am-u-sm-9">
          <input type="text" id="user-name" value="{{ $user->username }}" placeholder="姓名 / Name">
          <small>输入你的名字，让我们记住你。</small>
        </div>
      </div>

      <div class="am-form-group">
        <label for="user-email" class="am-u-sm-3 am-form-label">电子邮件 / Email</label>
        <div class="am-u-sm-9">
          <input type="email" id="user-email" value="{{ $user->email }}" placeholder="输入你的电子邮件 / Email">
          <small>邮箱你懂得...</small>
        </div>
      </div>

      <div class="am-form-group">
        <label for="user-phone" class="am-u-sm-3 am-form-label">电话 / Telephone</label>
        <div class="am-u-sm-9">
          <input type="tel" id="user-phone" value="{{ $user->phone }}" placeholder="输入你的电话号码 / Telephone">
        </div>
      </div>

      <div class="am-form-group">
        <div class="am-u-sm-9 am-u-sm-push-3">
          <button type="button" class="am-btn am-btn-primary">保存修改</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection