@extends('admin.layouts.master')

@section('title', 'Assigment')

@section('content-body')
<div class="am-cf am-padding">
  <ol class="am-breadcrumb">
    @include('admin.assignments.breadcrumb')
    <li class="am-active">新增</li>
  </ol>
</div>

<form action="{{ route('v3.assignments.store') }}" method="POST">
    {{ csrf_field() }}
    <div class="am-tabs am-margin" data-am-tabs>
        <ul class="am-tabs-nav am-nav am-nav-tabs">
          <li class="am-active"><a href="#tab1">基本信息</a></li>
        </ul>

        <div class="am-tabs-bd">
            <div class="am-tab-panel am-fade am-in am-active" id="tab1">
                <div class="am-g am-margin-top">
                  <div class="am-u-sm-4 am-u-md-2 am-text-right">
                    标题
                  </div>
                  <div class="am-u-sm-8 am-u-md-4">
                    <input type="text" name="title" class="am-input-sm" value="{{ session()->getOldInput('title') }}">
                  </div>
                  <div class="am-hide-sm-only am-u-md-6">
                  @if ($error = $errors->first('title'))
                    {{ $error }}
                  @else
                    *必填，不可重复
                  @endif
                  </div>
                </div>
            </div>
        </div>

        <div class="am-margin">
          <button type="submit" class="am-btn am-btn-primary am-btn-xs">提交保存</button>
        </div>
    </div>
</form>
@endsection