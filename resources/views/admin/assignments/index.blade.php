@extends('admin.layouts.master')

@section('title', 'Assignment')

@section('content-body')
<div class="am-cf am-padding">
  <ol class="am-breadcrumb">
    @include('admin.assignments.breadcrumb')
    <li class="am-active">列表</li>
  </ol>
</div>

<div class="am-g">
  <div class="am-u-sm-12 am-u-md-6">
    <div class="am-btn-toolbar">
      <div class="am-btn-group am-btn-group-xs">
        <button type="button" id="btn-create" class="am-btn am-btn-default"><span class="am-icon-plus"></span> 新增</button>
      </div>
    </div>
  </div>

  <div class="am-u-sm-12 am-u-md-3">
    <div class="am-input-group am-input-group-sm">
      <input type="text" id="q-keyword" class="am-form-field">
      <span class="am-input-group-btn">
        <button class="am-btn am-btn-default" id="btn-search" type="button">搜索</button>
      </span>
    </div>
  </div>
</div>

<div class="am-g">
  <div class="am-u-sm-12">
    <table class="am-table am-table-striped am-table-hover table-main">
      <thead>
      <tr>
        <th><input type="checkbox" /></th>
        <th>ID</th>
        <th>Title</th>
        <th>操作</th>
      </tr>
      </thead>
      <tbody>
      @foreach ($lists as $list)
      <tr class="lists-{{ $list->id }}">
        <td><input type="checkbox" /></td>
        <td>{{ $list->id }}</td>
        <td>{{ $list->title }}</td>
        <td>
          <div class="am-btn-toolbar">
            <div class="am-btn-group am-btn-group-xs">
              <button data-id="{{ $list->id }}" class="am-btn am-btn-default am-btn-xs am-text-secondary btn-edit"><span class="am-icon-pencil-square-o"></span> 编辑</button>
              <button data-id="{{ $list->id }}" class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only btn-remove"><span class="am-icon-trash-o"></span> 删除</button>
            </div>
          </div>
        </td>
      </tr>
      @endforeach
      </tbody>
    </table>
    <div class="am-cf">
      共 <span id="list-count">{{ $lists->count() }}</span> 条记录
      <div class="am-fr">
      {!! $pagination !!}
      </div>
    </div>
    <hr />
    <p>注：.....</p>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="/admin/layer/layer.js"></script>
<script type="text/javascript">
    $('#btn-create').click(function() {
        location.href = '{{ route("b.assignments.create") }}';
    });

    $('button.btn-edit').click(function() {
      var id = $(this).data('id');

      location.href = '/b/assignments/' + id + '/edit';
    });

    $('button.btn-remove').click(function() {
      var id = $(this).data('id');

      layer.confirm('确定删除吗？', {
        btn: ['是','否']
      }, function(index) {
        $.ajax({
          type: 'DELETE',
          url: '/b/assignments/' + id,
          success: function(data) {
            if (! data.error) {
              $('.lists-' + id).remove();
            } else {
              layer.msg(data.error_description);
            }
          }
        });
        // close layer
        layer.close(index);
      });
    });
</script>
@endsection