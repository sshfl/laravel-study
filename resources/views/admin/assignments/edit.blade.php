@extends('admin.layouts.master')

@section('title', 'Assignment')

@section('content-body')
<div class="am-cf am-padding">
  <ol class="am-breadcrumb">
    @include('admin.assignments.breadcrumb')
    <li class="am-active">编辑</li>
  </ol>
</div>

<form action="{{ route('v3.assignments.update', $id) }}" method="POST">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="am-tabs am-margin" data-am-tabs>
        <ul class="am-tabs-nav am-nav am-nav-tabs">
          <li class="am-active"><a href="#tab1">基本信息</a></li>
        </ul>

        <div class="am-tabs-bd">
            <div class="am-tab-panel am-fade am-in am-active" id="tab1">
                <div class="am-g am-margin-top">
                  <div class="am-u-sm-4 am-u-md-2 am-text-right">
                    标题
                  </div>
                  <div class="am-u-sm-8 am-u-md-4">
                    <input type="text" name="title" value="{{ $assignment->title }}" class="am-input-sm">
                  </div>
                  <div class="am-hide-sm-only am-u-md-6">{{ $errors->first('title') }}</div>
                </div>
            </div>
        </div>

        <div class="am-margin">
          <button type="submit" class="am-btn am-btn-primary am-btn-xs">提交保存</button>
        </div>
    </div>
</form>
@endsection