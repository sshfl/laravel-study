@extends('admin.layouts.master')

@section('title', 404)

@section('content-body')
<div class="am-cf am-padding">
  <ol class="am-breadcrumb">
    @include('admin.layouts.breadcrumb')
    <li class="am-active">404</li>
  </ol>
</div>


<div class="am-g">
    <div class="am-u-sm-12">
    <h2 class="am-text-center am-text-xxxl am-margin-top-lg">404. Not Found</h2>
    <p class="am-text-center">没有找到你要的页面</p>
    <pre class="page-404">
          .----.
       _.'__    `.
   .--($)($$)---/#\
 .' @          /###\
 :         ,   #####
  `-..__.-' _.-\###/
        `;_:    `"'
      .'"""""`.
     /,  ya ,\\
    //  404!  \\
    `-._______.-'
    ___`. | .'___
   (______|______)
    </pre>
    </div>
</div>
@endsection