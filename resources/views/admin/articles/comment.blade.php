<h1>Article Comment</h1>

<form action="/b/articles/{{ $id }}/comments" method="POST">
    {{ csrf_field() }}
    user_id: <input type="text" name="user_id" value="11">
    <span>{{ $errors->first('user_id') }}</span>
    <br />
    <br />
    content: <input type="text" name="content" value="{{ session()->getOldInput('content') }}">
    <span>{{ $errors->first('content') }}</span>
    <br />
    <br />
    <input type="submit">
</form>