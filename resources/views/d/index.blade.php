<h1>你对什么感兴趣？</h1>
<h2>选择想要看到的内容吧</h2>

<hr />

<form action="/d/interests" method="POST">
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    <div>
        书影音:
        <input type="checkbox" value="影视">影视
        <input type="checkbox" value="读书">读书
    </div>

    <hr />

    <div>
        会生活:
        <input type="checkbox" value="旅行">旅行
        <input type="checkbox" value="居家">居家
        <input type="checkbox" value="美食">美食
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" value="手作">手作
        <input type="checkbox" value="运动健身">运动健身
        <input type="checkbox" value="时尚">时尚
    </div>

    <hr />

    <div>
        看世界:
        <input type="checkbox" value="人文">人文
        <input type="checkbox" value="科技">科技
        <input type="checkbox" value="摄影">摄影
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" value="艺术">艺术
        <input type="checkbox" value="画画儿">画画儿
        <input type="checkbox" value="建筑">建筑
    </div>

    <hr />

    <div>
        在成长:
        <input type="checkbox" value="故事">故事
        <input type="checkbox" value="情感">情感
        <input type="checkbox" value="成长">成长
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" value="涨知识">涨知识
        <input type="checkbox" value="理财">理财
    </div>

    <hr />

    <div>
        有意思:
        <input type="checkbox" value="找乐">找乐
        <input type="checkbox" value="宠物">宠物
        <input type="checkbox" value="娱乐八卦">娱乐八卦
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" value="动漫">动漫
        <input type="checkbox" value="自然">自然
        <input type="checkbox" value="美女">美女
    </div>

    <hr />

    <input type="submit" value="选好了，保存>">
</form>