<h1>Basic Setting</h1>

@if (count($errors) > 0)
    <div>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif

<form action="/accounts/1129" method="POST">
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    名号：
    <input type="text" name="name_alias" value="{{ $user->name_alias }}">
    <br />
    名号30天内只能修改一次。
    <br />
    我的域名:
    <input type="text" name="domain_name" value="{{ $user->domain_name }}">
    <br />
    最多15个字符，字母开头，设置后不能修改
    <br />
    <br />
    <input type="submit" value="更新设置">
</form>